package com.drugs.manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.function.RouterFunction;
import org.springframework.web.servlet.function.ServerResponse;

import javax.ws.rs.ApplicationPath;
import java.net.URI;

import static org.springframework.web.servlet.function.RequestPredicates.GET;
import static org.springframework.web.servlet.function.RouterFunctions.route;

@SpringBootApplication
@ApplicationPath("drug-records")
public class DrugRecordsManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DrugRecordsManagerApplication.class, args);
    }


    @Bean
    RouterFunction<ServerResponse> routerFunction() {
        return route(GET("/drug-records"), req ->
                ServerResponse.temporaryRedirect(URI.create("swagger-ui.html")).build());
    }

}
