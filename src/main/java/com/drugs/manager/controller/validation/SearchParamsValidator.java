package com.drugs.manager.controller.validation;

import com.drugs.manager.model.search.DrugRecordSearchRequest;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SearchParamsValidator {
    private final List<SearchRequestValidator> requestValidators;

    public SearchParamsValidator(List<SearchRequestValidator> requestValidators) {
        this.requestValidators = requestValidators;
    }

    public void validate(DrugRecordSearchRequest searchRequest) {
        requestValidators.forEach(validator -> validator.validate(searchRequest));
    }
}
