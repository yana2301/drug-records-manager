package com.drugs.manager.controller.validation;

import com.drugs.manager.model.search.DrugRecordSearchRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class FdaBrandNameValidator implements SearchRequestValidator {
    @Override
    public void validate(DrugRecordSearchRequest searchRequest) {
        if (searchRequest.getFdaBrandName() != null && StringUtils.isBlank(searchRequest.getFdaBrandName())) {
            throw new IllegalArgumentException("FDA brand name cannot contain only spaces");
        }
    }
}
