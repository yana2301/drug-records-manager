package com.drugs.manager.controller.validation;

import com.drugs.manager.model.search.DrugRecordSearchRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class FdaManufacturerNameValidator implements SearchRequestValidator {

    @Override
    public void validate(DrugRecordSearchRequest searchRequest) {
        if (StringUtils.isBlank(searchRequest.getFdaManufacturerName())) {
            throw new IllegalArgumentException("FDA Manufacturer name cannot be blank");
        }
    }
}
