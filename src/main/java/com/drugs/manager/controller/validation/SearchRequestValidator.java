package com.drugs.manager.controller.validation;

import com.drugs.manager.model.search.DrugRecordSearchRequest;

public interface SearchRequestValidator {
    void validate(DrugRecordSearchRequest searchRequest);
}
