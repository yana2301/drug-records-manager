package com.drugs.manager.controller;

import com.drugs.manager.dao.DrugRecordsRepository;
import com.drugs.manager.model.DrugRecord;
import com.drugs.manager.model.exception.DrugRecordAlreadyExistsException;
import com.drugs.manager.model.exception.DrugRecordNotFoundException;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("manage")
public class DrugRecordsCRUDController {
    private final DrugRecordsRepository drugRecordsRepository;

    public DrugRecordsCRUDController(DrugRecordsRepository drugRecordsRepository) {
        this.drugRecordsRepository = drugRecordsRepository;
    }

    @GetMapping("/{application-number}")
    @ApiOperation(value = "Returns drug record by application number", response = DrugRecord.class)
    public DrugRecord getById(@PathVariable("application-number") String id) {
        return drugRecordsRepository.findById(id).orElseThrow(() -> new DrugRecordNotFoundException(id));
    }

    @GetMapping("/")
    @ApiOperation(value = "Lists all drug records", response = List.class)
    public List<DrugRecord> getDrugRecords(@RequestParam(value = "offset", defaultValue = "0", required = false) int offset,
                                           @RequestParam(value = "limit", defaultValue = "10", required = false) int limit) {
        return drugRecordsRepository.listDrugRecords(offset, limit);
    }

    @PostMapping("/create")
    @ApiOperation(value = "Creates drug record", response = String.class)
    public String createDrugRecord(DrugRecord drugRecord) {
        if (drugRecordsRepository.existsById(drugRecord.getApplicationNumber())) {
            throw new DrugRecordAlreadyExistsException(drugRecord.getApplicationNumber());
        } else {
            return drugRecordsRepository.save(drugRecord).getApplicationNumber();
        }
    }

    @PutMapping("/update")
    @ApiOperation(value = "Updates drug record")
    public void updateDrugRecord(DrugRecord drugRecord) {
        validateDrugRecordExists(drugRecord.getApplicationNumber());
        drugRecordsRepository.save(drugRecord);
    }

    @DeleteMapping("/{application-number}")
    @ApiOperation(value = "Deletes drug record")
    public void deleteDrugRecord(@PathVariable("application-number") String id) {
        validateDrugRecordExists(id);
        drugRecordsRepository.deleteById(id);
    }

    public void validateDrugRecordExists(String drugRecordId) {
        if (!drugRecordsRepository.existsById(drugRecordId)) {
            throw new DrugRecordNotFoundException(drugRecordId);
        }
    }
}
