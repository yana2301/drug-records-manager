package com.drugs.manager.controller;

import com.drugs.manager.controller.validation.SearchParamsValidator;
import com.drugs.manager.model.search.DrugRecordSearchRequest;
import com.drugs.manager.model.search.DrugRecordsSearchResponse;
import com.drugs.manager.service.search.DrugRecordsSearchService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/fda")
public class DrugRecordsSearchController {
    private final SearchParamsValidator searchParamsValidator;

    private final DrugRecordsSearchService drugRecordsSearchService;

    public DrugRecordsSearchController(SearchParamsValidator searchParamsValidator,
                                       DrugRecordsSearchService drugRecordsSearchService) {
        this.searchParamsValidator = searchParamsValidator;
        this.drugRecordsSearchService = drugRecordsSearchService;
    }

    @GetMapping("/search")
    @ApiOperation(value = "Searches drug submissions in FDA database", response = DrugRecordsSearchResponse.class)
    public DrugRecordsSearchResponse searchDrugRecords(@RequestParam(value = "fda_manufacturer_name") String fdaManufacturerName,
                                                       @RequestParam(value = "fda_brand_name", required = false) String fdaBrandName,
                                                       @RequestParam(value = "offset", required = false, defaultValue = "0") int offset,
                                                       @RequestParam(value = "limit", required = false, defaultValue = "10") int limit) {
        DrugRecordSearchRequest recordSearchRequest = new DrugRecordSearchRequest(fdaManufacturerName, fdaBrandName, offset, limit);
        searchParamsValidator.validate(recordSearchRequest);
        return drugRecordsSearchService.searchDrugRecords(recordSearchRequest);
    }
}
