package com.drugs.manager.service.search;

import com.drugs.manager.config.ApplicationConfiguration;
import com.drugs.manager.model.exception.InternalSearchErrorException;
import com.drugs.manager.model.exception.InvalidSearchRequestException;
import com.drugs.manager.model.search.DrugRecordSearchRequest;
import com.drugs.manager.model.search.DrugRecordsSearchResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
public class DrugRecordsSearchService {
    public static final String ERROR_LOG_TEMPLATE = "Request to search FDA records failed. Search request: {}";
    private final RestTemplate restTemplate;

    private final ApplicationConfiguration applicationConfiguration;

    private final SearchParameterBuilder searchParameterBuilder;

    public DrugRecordsSearchService(RestTemplateBuilder restTemplateBuilder,
                                    ApplicationConfiguration applicationConfiguration,
                                    SearchParameterBuilder searchParameterBuilder) {
        restTemplate = restTemplateBuilder.build();
        this.applicationConfiguration = applicationConfiguration;
        this.searchParameterBuilder = searchParameterBuilder;
    }

    public DrugRecordsSearchResponse searchDrugRecords(DrugRecordSearchRequest searchRequest) {
        try {
            String searchString = searchParameterBuilder.fromRequest(searchRequest);
            ResponseEntity<DrugRecordsSearchResponse> searchResponse = restTemplate.getForEntity(
                    applicationConfiguration.getSearchDrugRecordsUrl(),
                    DrugRecordsSearchResponse.class,
                    searchString,
                    searchRequest.getOffset(),
                    searchRequest.getLimit());
            if (searchResponse.getStatusCode().equals(HttpStatus.OK)) {
                return searchResponse.getBody();
            } else {
                throw new InternalSearchErrorException(searchRequest);
            }
        } catch (HttpClientErrorException e) {
            log.error(ERROR_LOG_TEMPLATE, searchRequest, e);
            if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                return DrugRecordsSearchResponse.empty();
            } else if (e.getStatusCode().is4xxClientError()) {
                throw new InvalidSearchRequestException(searchRequest, e.getMessage());
            } else {
                throw new InternalSearchErrorException(searchRequest);
            }
        } catch (Exception e) {
            log.error(ERROR_LOG_TEMPLATE, searchRequest, e);
            throw new InternalSearchErrorException(searchRequest);
        }
    }
}
