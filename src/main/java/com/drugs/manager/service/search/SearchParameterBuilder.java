package com.drugs.manager.service.search;

import com.drugs.manager.model.search.DrugRecordSearchRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class SearchParameterBuilder {
    private static final String OPENFDA_MANUFACTURER_NAME = "openfda.manufacturer_name";
    private static final String OPENFDA_BRAND_NAME = "openfda.brand_name";

    public String fromRequest(DrugRecordSearchRequest drugRecordSearchRequest) {
        StringBuilder searchRequest = new StringBuilder();
        searchRequest.append(OPENFDA_MANUFACTURER_NAME).append(":").append(drugRecordSearchRequest.getFdaManufacturerName());
        if (StringUtils.isNotEmpty(drugRecordSearchRequest.getFdaBrandName())) {
            searchRequest.append(" AND ").append(OPENFDA_BRAND_NAME).append(":").append(drugRecordSearchRequest.getFdaBrandName());
        }
        return searchRequest.toString();
    }
}
