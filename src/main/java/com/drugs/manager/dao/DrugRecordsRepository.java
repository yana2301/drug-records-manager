package com.drugs.manager.dao;

import com.drugs.manager.model.DrugRecord;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DrugRecordsRepository extends CrudRepository<DrugRecord, String> {
    @Query(value = "SELECT * FROM drug_records records  ORDER BY records.application_number offset ?1 limit ?2", nativeQuery = true)
    List<DrugRecord> listDrugRecords(int offset, int limit);

}
