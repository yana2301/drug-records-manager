package com.drugs.manager.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:application.properties")
@Getter
@Configuration
public class ApplicationConfiguration {
    @Value("${search.drug_records_url}")
    private String searchDrugRecordsUrl;
}
