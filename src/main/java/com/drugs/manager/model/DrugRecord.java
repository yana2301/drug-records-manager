package com.drugs.manager.model;

import com.drugs.manager.dao.ListConverter;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "drug_records")
public class DrugRecord {
    @Id
    @Column(name = "application_number", nullable = false)
    String applicationNumber;

    @Column(name = "manufacturer_name", nullable = false)
    String manufacturerName;

    @Column(name = "substance_name", nullable = false)
    String substanceName;

    @Column(name = "product_numbers", nullable = false)
    @Convert(converter = ListConverter.class)
    List<String> productNumbers;
}
