package com.drugs.manager.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class DrugRecordNotFoundException extends ResponseStatusException {
    public DrugRecordNotFoundException(String id) {
        super(HttpStatus.NOT_FOUND, composeErrorMessage(id));
    }

    private static String composeErrorMessage(String id) {
        return String.format("Entity with id: %s not found", id);
    }
}
