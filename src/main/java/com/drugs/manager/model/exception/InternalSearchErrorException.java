package com.drugs.manager.model.exception;

import com.drugs.manager.model.search.DrugRecordSearchRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class InternalSearchErrorException extends ResponseStatusException {
    public InternalSearchErrorException(DrugRecordSearchRequest searchRequest) {
        super(HttpStatus.INTERNAL_SERVER_ERROR, composeErrorMessage(searchRequest));
    }

    private static String composeErrorMessage(DrugRecordSearchRequest searchRequest) {
        return "Request to search FDA records failed. Please retry later. Search request: " + searchRequest.toString();
    }
}
