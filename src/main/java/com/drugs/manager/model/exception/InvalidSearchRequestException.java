package com.drugs.manager.model.exception;

import com.drugs.manager.model.search.DrugRecordSearchRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidSearchRequestException extends ResponseStatusException {
    public InvalidSearchRequestException(DrugRecordSearchRequest searchRequest, String errorMessage) {
        super(HttpStatus.BAD_REQUEST, composeErrorMessage(searchRequest, errorMessage));
    }

    private static String composeErrorMessage(DrugRecordSearchRequest searchRequest, String errorMessage) {
        return String.format("Search Request with parameters %s failed due to error %s", searchRequest.toString(), errorMessage);
    }
}
