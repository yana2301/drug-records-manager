package com.drugs.manager.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class DrugRecordAlreadyExistsException extends ResponseStatusException {
    public DrugRecordAlreadyExistsException(String id) {
        super(HttpStatus.BAD_REQUEST, String.format("Drug record with id %s already exists", id));
    }
}
