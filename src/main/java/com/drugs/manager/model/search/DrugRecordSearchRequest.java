package com.drugs.manager.model.search;

import lombok.Value;

@Value
public class DrugRecordSearchRequest {
    String fdaManufacturerName;
    String fdaBrandName;
    int offset;
    int limit;

    @Override
    public String toString() {
        return "FDA Manufacturer name = " + fdaManufacturerName +
                "FDA Brand Name = " + fdaBrandName +
                "Limit = " + limit +
                "Offset = " + offset;
    }
}
