package com.drugs.manager.model.search;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
public class DrugRecordSubmission {
    public static final String SEPARATOR = ",";
    @JsonProperty("application_number")
    String applicationNumber;

    String brandName;

    String manufacturerName;

    String substanceName;

    List<String> productNumbers;

    @JsonProperty("products")
    private void unpackProductNumbers(List<Map<String, Object>> products) {
        productNumbers = products.stream().map(next -> String.valueOf(next.get("product_number"))).collect(Collectors.toList());
    }

    @JsonProperty("openfda")
    private void unpackSubmissionDetails(Map<String, Object> openFDA) {
        brandName = listToString(openFDA.get("brand_name"));
        manufacturerName = listToString(openFDA.get("manufacturer_name"));
        substanceName = listToString(openFDA.get("substance_name"));
    }

    private String listToString(Object value) {
        List valueList = (List) value;
        return CollectionUtils.isNotEmpty(valueList) ? StringUtils.join(valueList, SEPARATOR) : null;
    }
}
