package com.drugs.manager.model.search;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collections;
import java.util.List;

@Data
@AllArgsConstructor
public class DrugRecordsSearchResponse {
    @JsonProperty("results")
    List<DrugRecordSubmission> drugRecordSubmissions;

    @JsonProperty("meta")
    Paging paging;

    public static DrugRecordsSearchResponse empty() {
        return new DrugRecordsSearchResponse(Collections.emptyList(), new Paging());
    }
}
