package com.drugs.manager.model.search;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.apache.commons.collections4.MapUtils;

import java.util.Map;

@Data
public class Paging {
    int offset;
    int limit;
    int total;

    @JsonProperty("results")
    private void unpackPagination(Map<String, Integer> results) {
        if (MapUtils.isNotEmpty(results)) {
            this.offset = results.get("skip");
            this.limit = results.get("limit");
            this.total = results.get("total");
        }
    }
}
