package com.drugs.manager.service.search;

import com.drugs.manager.config.ApplicationConfiguration;
import com.drugs.manager.model.exception.InternalSearchErrorException;
import com.drugs.manager.model.exception.InvalidSearchRequestException;
import com.drugs.manager.model.search.DrugRecordSearchRequest;
import com.drugs.manager.model.search.DrugRecordSubmission;
import com.drugs.manager.model.search.DrugRecordsSearchResponse;
import com.drugs.manager.model.search.Paging;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DrugRecordsSearchServiceTest {
    public static final String APPLICATION_NUMBER = "1111";
    public static final String SEARCH_URL = "search_url";
    public static final String SEARCH_STRING = "search_string";
    public static final int OFFSET = 0;
    public static final int LIMIT = 10;
    @Mock
    private RestTemplateBuilder restTemplateBuilder;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private ApplicationConfiguration applicationConfiguration;

    @Mock
    private SearchParameterBuilder searchParameterBuilder;

    private DrugRecordsSearchService sut;

    @Before
    public void setUp() {
        when(restTemplateBuilder.build()).thenReturn(restTemplate);
        when(searchParameterBuilder.fromRequest(any(DrugRecordSearchRequest.class))).thenReturn(SEARCH_STRING);
        when(applicationConfiguration.getSearchDrugRecordsUrl()).thenReturn(SEARCH_URL);
        sut = new DrugRecordsSearchService(restTemplateBuilder, applicationConfiguration, searchParameterBuilder);
    }

    @Test
    public void shouldReturnConvertedResponse() {
        ResponseEntity<DrugRecordsSearchResponse> responseEntity = ResponseEntity.of(Optional.of(createDrugSearchResponse()));

        when(restTemplate.getForEntity(SEARCH_URL, DrugRecordsSearchResponse.class, SEARCH_STRING, OFFSET, LIMIT))
                .thenReturn(responseEntity);

        assertEquals(createDrugSearchResponse(), sut.searchDrugRecords(searchRequest()));
    }

    @Test
    public void shouldReturnEmptyResponseIfFDAThrowsNotFoundException() {
        when(restTemplate.getForEntity(SEARCH_URL, DrugRecordsSearchResponse.class, SEARCH_STRING, OFFSET, LIMIT))
                .thenThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND));

        assertEquals(DrugRecordsSearchResponse.empty(), sut.searchDrugRecords(searchRequest()));
    }

    @Test(expected = InvalidSearchRequestException.class)
    public void shouldThrowInvalidSearchRequestExceptionIf4xxErrorIsThrown() {
        when(restTemplate.getForEntity(SEARCH_URL, DrugRecordsSearchResponse.class, SEARCH_STRING, OFFSET, LIMIT))
                .thenThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST));
        sut.searchDrugRecords(searchRequest());
    }

    @Test(expected = InternalSearchErrorException.class)
    public void shouldThrowSearchRequestFailedExceptionIfEntityStatusNot200() {
        ResponseEntity<DrugRecordsSearchResponse> responseEntity = ResponseEntity.badRequest().build();

        when(restTemplate.getForEntity(SEARCH_URL, DrugRecordsSearchResponse.class, SEARCH_STRING, OFFSET, LIMIT))
                .thenReturn(responseEntity);

        sut.searchDrugRecords(searchRequest());
    }

    @Test(expected = InternalSearchErrorException.class)
    public void shouldThrowSearchRequestFailedExceptionIfInternalServerErrorIsThrown() {
        when(restTemplate.getForEntity(SEARCH_URL, DrugRecordsSearchResponse.class, SEARCH_STRING, OFFSET, LIMIT))
                .thenThrow(new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR));
        sut.searchDrugRecords(searchRequest());

    }

    private DrugRecordsSearchResponse createDrugSearchResponse() {
        DrugRecordSubmission drugRecordSubmission = new DrugRecordSubmission();
        drugRecordSubmission.setApplicationNumber(APPLICATION_NUMBER);
        return new DrugRecordsSearchResponse(Collections.singletonList(drugRecordSubmission), new Paging());
    }

    private DrugRecordSearchRequest searchRequest() {
        return new DrugRecordSearchRequest("manufacturer1", null, OFFSET, LIMIT);
    }
}
