# Drug Records Manager Application

### Overview

Drug Records Manager Applications allows to search and manage drug record submissions. Documentation for the
application's endpoints is available at http://localhost:8080/drug-records

### Requirements

In order to run the Application, following software should be installed:

* Java Runtime Environment 11 or later
* PostgreSQL database

### How to Build and Start the Application

1. Specify valid database credentials in application.properties file.
2. Build jar file by command
   ``mvn clean install``.
   Please note that in order to build application from sources, Maven should be installed.
3. Navigate to folder with built jar file(target folder) and run command: 
   ``java -jar manager-0.0.1-SNAPSHOT.jar com.drugs.manager.DrugRecordsManagerApplication``
4. In browser open link ``http://localhost:8080/drug-records``
